class Word
  # A method to check palindrome strings.
  def palindrome?(string)
    string == string.reverse
  end
end

w = Word.new                  # Create a new object of the Word class.
puts w.palindrome?("foobar")  # Should return false.
puts w.palindrome?("level")   # Should return true.
