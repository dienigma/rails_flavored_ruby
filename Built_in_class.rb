class String
  # Returns true if the string is a palindrome
  def palindrome?
    self == self.reverse
  end
end

puts "deified".palindrome?
puts "racecar".palindrome?
puts "onomatopoeia".palindrome?
